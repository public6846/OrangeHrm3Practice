package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuzzPage {
    // Buzz main page

    @FindBy(xpath = "//p[text() = 'Buzz Newsfeed']/..//textarea")
    public WebElement textAreaBuzzComment;

    @FindBy(xpath = "//button[.//span[contains(@class, 'cameraglass')]]")
    public WebElement btnBuzzSharePhoto;

    @FindBy(xpath = "//p[text() = 'Share Photos']/../..//input[@type='file']")
    public WebElement fileBuzzAddPhoto;

    @FindBy(xpath = "//p[text() = 'Share Photos']/../..//button[@type = 'submit']")
    public WebElement btnBuzzShare;

    @FindBy(xpath = "//p[text()='BuzzTest123']/../../..//div[@class = 'orangehrm-buzz-post-actions']/div")
    public WebElement btnBuzzLike;

    @FindBy(xpath = "//p[text()='BuzzTest123']/../../..//div[@class = 'orangehrm-buzz-stats']/div/i/../p")
    public WebElement textBuzzAfterLike;

    @FindBy(xpath = "//p[text()='BuzzTest123']/../../..//i[@class = 'oxd-icon bi-three-dots']/..")
    public WebElement btnBuzzThreeDots;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//i[@class = 'oxd-icon bi-three-dots']/..")
    public WebElement btnBuzzThreeDotsEdited;

    @FindBy(xpath = "//li[contains(@class, 'buzz')][.//i[@class='oxd-icon bi-pencil']]")
    public WebElement btnBuzzThreeDotsEdit;

    @FindBy(xpath = "//li[contains(@class, 'buzz')][.//i[@class='oxd-icon bi-trash']]")
    public WebElement btnBuzzThreeDotsDelete;

    @FindBy(xpath = "//div[@class='orangehrm-modal-header']")
    public WebElement elementBuzzEditModal;

    @FindBy(xpath = "//p[text() = 'Edit Post']/../..//textarea")
    public WebElement textAreaBuzzEditModalComment;

    @FindBy(xpath = "//p[text() = 'Edit Post']/../..//button[@type='submit']")
    public WebElement btnBuzzEditModalPost;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//i[@class='oxd-icon bi-chat-text-fill']/..")
    public WebElement btnBuzzComment;

    @FindBy(xpath = "//div[@class='orangehrm-buzz-comment']//input")
    public WebElement fieldBuzzCommentText;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//p[text()='Like']")
    public WebElement btnBuzzCommentLike;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//i[@class= 'oxd-icon bi-heart-fill orangehrm-post-comment-stats-icon']")
    public WebElement elementBuzzCommentLikeHeart;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//p[text()='Edit']")
    public WebElement btnBuzzCommentEdit;

    @FindBy(xpath = "//p[text()='BuzzTest123Edit']/../../..//p[text()='Delete']")
    public WebElement btnBuzzCommentDelete;

    @FindBy(xpath = "//div[@class='orangehrm-post-comment']//input")
    public WebElement fieldBuzzCommentEdit;

    @FindBy(xpath = "//div[@class='orangehrm-modal-footer']/button[contains(@class, 'oxd-button--label-danger')]")
    public WebElement btnBuzzCommentModalDelete;

    @FindBy(xpath = "//div[@id='oxd-toaster_1']//p[contains(@class,'toast-message')]")
    public WebElement textBuzzPostDeleteToast;


    public BuzzPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
