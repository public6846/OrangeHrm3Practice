package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

// page_url = https://opensource-demo.orangehrmlive.com
public class MainPage {

    // Login

    @FindBy(xpath = "//input[@name='username']")
    public WebElement fieldUsername;

    @FindBy(xpath = "//input[@name='password']")
    public WebElement fieldPassword;

    @FindBy(xpath = "//button[@type= 'submit']")
    public WebElement btnLogin;

    // Dashboard

    @FindBy(xpath = "//span[@class='oxd-userdropdown-tab']")
    public WebElement btnUserInfo;

    @FindBy(xpath = "//a[contains(@href, 'PimModule')]")
    public WebElement btnMenuPim;

    @FindBy(xpath = "//a[contains(@href, 'RecruitmentModule')]")
    public WebElement btnMenuRecruitment;

    @FindBy(xpath = "//a[contains(@href, 'viewBuzz')]")
    public WebElement btnMenuBuzz;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
