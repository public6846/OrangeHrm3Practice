package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MainPageTest {
    private static WebDriver driver;
    private MainPage mainPage;
    private PimPage pimPage;
    private RecruitmentPage recPage;
    private BuzzPage buzzPage;
    private ReportPage reportPage;


    @BeforeMethod
    public void setUp() {
//        ChromeOptions options = new ChromeOptions();
//        // Fix the issue https://github.com/SeleniumHQ/selenium/issues/11750
//        options.addArguments("--remote-allow-origins=*");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://opensource-demo.orangehrmlive.com");

        mainPage = new MainPage(driver);
        pimPage = new PimPage(driver);
        recPage = new RecruitmentPage(driver);
        buzzPage = new BuzzPage(driver);
        reportPage = new ReportPage(driver);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void login() {
        waitElementToLoad(mainPage.fieldUsername);
        mainPage.fieldUsername.sendKeys("Admin");
        mainPage.fieldPassword.sendKeys("admin123");
        mainPage.btnLogin.click();
        waitElementToLoad(mainPage.btnUserInfo);


/*      WebElement searchPageField = driver.findElement(By.cssSelector("input[data-test='search-input']"));
        assertEquals(searchPageField.getAttribute("value"), "Selenium");*/
    }


    @Test
    public void createEmployee() throws InterruptedException {
        login();
        // dashboard
        waitElementToLoad(mainPage.btnMenuPim);
        mainPage.btnMenuPim.click();
        // list
        waitElementToLoad(pimPage.btnAddPim);
        pimPage.btnAddPim.click();
        // form
        String filePath = System.getProperty("user.dir") + TestData.PROFILE_PICTURE_PATH;
        pimPage.fileAddPicture.sendKeys(filePath);
        pimPage.fieldPimFirstName.sendKeys(TestData.EMPLOYEE_FIRST_NAME);
        pimPage.fieldPimLastName.sendKeys(TestData.EMPLOYEE_LAST_NAME);
        fluentWaitElementInvisible(pimPage.elementLoader);

        pimPage.switchLoginDetails.click();
        waitElementToLoad(pimPage.fieldPimUsername);
        pimPage.fieldPimUsername.sendKeys(TestData.EMPLOYEE_USERNAME);
        pimPage.fieldPimPassword.sendKeys(TestData.EMPLOYEE_PASSWORD);
        pimPage.fieldPimPasswordConfirm.sendKeys(TestData.EMPLOYEE_PASSWORD);
        pimPage.btnPimSave.click();

        // Job details
        fluentWaitElementInvisible(pimPage.elementLoader);
        waitElementToLoad(pimPage.btnPimJob);
        pimPage.btnPimJob.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        pimPage.fieldPimJoinDate.sendKeys(TestData.EMPLOYEE_JOIN_DATE);
        pimPage.fieldPimJobTitle.click();
        waitElementToLoad(pimPage.btnPimJobTitleCEO);
        pimPage.btnPimJobTitleCEO.click();
        pimPage.fieldPimJobCategory.click();
        waitElementToLoad(pimPage.btnPimJobCategoryCW);
        pimPage.btnPimJobCategoryCW.click();
        pimPage.fieldPimJobLocation.click();
        waitElementToLoad(pimPage.btnPimJobLocationNYSO);
        pimPage.btnPimJobLocationNYSO.click();
        pimPage.fieldPimEmploymentStatus.click();
        waitElementToLoad(pimPage.btnPimEmploymentStatusFTP);
        pimPage.btnPimEmploymentStatusFTP.click();
        pimPage.btnPimJobDetailsSave.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        pimPage.btnPimReportTo.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // Report-to form
        // ToDo: enable when all employees are not deleted
/*        pimPage.btnReportAddSupervisor.click();
        waitElementToLoad(pimPage.fieldReportSupervisorName);
        pimPage.fieldReportSupervisorName.sendKeys(TestData.SUPERVISOR_FULL_NAME);
        waitElementToLoad(pimPage.btnReportSupervisorNameOdis);
        pimPage.btnReportSupervisorNameOdis.click();
        pimPage.fieldReportSupervisorMethod.click();
        waitElementToLoad(pimPage.btnReportSupervisorMethodDirect);
        pimPage.btnReportSupervisorMethodDirect.click();
        pimPage.btnReportSupervisorSave.click();*/

        // Check PIM employee list
        mainPage.btnMenuPim.click();
        waitElementToLoad(pimPage.fieldPimEmploymentStatus);
        pimPage.fieldPimEmploymentStatus.click();
        waitElementToLoad(pimPage.btnPimEmploymentStatusFTP);
        pimPage.btnPimEmploymentStatusFTP.click();
        // ToDo: enable when all employees are not deleted
//        pimPage.fieldPimSupervisorName.sendKeys(TestData.SUPERVISOR_FULL_NAME);
//        waitElementToLoad(pimPage.btnListSupervisorNameOdis);
//        pimPage.btnListSupervisorNameOdis.click();
        pimPage.btnListPimSearch.click();
        Assert.assertEquals(pimPage.textPimEmployeeLastNameInList.getText(), TestData.EMPLOYEE_FIRST_NAME,
                "Assertion in PIM list failed, searched item in the list is not found. Names don't match");


    }

    @Test
    public void editEmployeeDetails() throws InterruptedException {
        login();
        // dashboard
        waitElementToLoad(mainPage.btnMenuPim);
        mainPage.btnMenuPim.click();
        // list
        waitElementToLoad(pimPage.btnAddPim);
        pimPage.btnAddPim.click();
        // form
        pimPage.fieldPimFirstName.sendKeys(TestData.EMPLOYEE_FIRST_NAME);
        pimPage.fieldPimLastName.sendKeys(TestData.EMPLOYEE_LAST_NAME);
        fluentWaitElementInvisible(pimPage.elementLoader);
        pimPage.btnPimSave.click();
        // Job details
        fluentWaitElementInvisible(pimPage.elementLoader);
        waitElementToLoad(pimPage.fieldPersonalFirstName);
        pimPage.fieldPersonalFirstName.clear();
        pimPage.fieldPersonalFirstName.sendKeys(TestData.EMPLOYEE_FIRST_NAME_EDITED);
        pimPage.fieldPersonalMiddleName.clear();
        pimPage.fieldPersonalMiddleName.sendKeys(TestData.EMPLOYEE_MIDDLE_NAME_EDITED);
        pimPage.fieldPersonalLastName.clear();
        pimPage.fieldPersonalLastName.sendKeys(TestData.EMPLOYEE_LAST_NAME_EDITED);
        pimPage.fieldPersonalNickname.sendKeys(TestData.EMPLOYEE_NICKNAME);
        pimPage.fieldPersonalOtherId.sendKeys(TestData.EMPLOYEE_OTHER_ID);
        pimPage.fieldPersonalDriverLicenseNumber.sendKeys(TestData.EMPLOYEE_DRIVER_LICENSE_NUMBER);
        pimPage.selectPersonalLicenseExpiry.sendKeys(TestData.EMPLOYEE_LICENSE_EXPIRY_DATE);
        pimPage.fieldPersonalSsnNumber.sendKeys(TestData.EMPLOYEE_SSN_NUMBER);
        pimPage.fieldPersonalSinNumber.sendKeys(TestData.EMPLOYEE_SIN_NUMBER);
        pimPage.selectPersonalNationality.click();
        waitElementToLoad(pimPage.btnPersonalNationalityAlbanian);
        pimPage.btnPersonalNationalityAlbanian.click();
        pimPage.selectPersonalMaritalStatus.click();
        waitElementToLoad(pimPage.btnPersonalMaritalStatusOther);
        pimPage.btnPersonalMaritalStatusOther.click();
        pimPage.fieldPersonalDateOfBirth.sendKeys(TestData.EMPLOYEE_DATE_OF_BIRTH);
        pimPage.radioPersonalGender.click();
        pimPage.fieldPersonalMilitaryService.sendKeys(TestData.EMPLOYEE_MILITARY_SERVICE);
        pimPage.checkPersonalSmoker.click();
        pimPage.btnPersonalSaveDetails.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        pimPage.selectPersonalBloodType.click();
        waitElementToLoad(pimPage.btnPersonalBloodTypeAPlus);
        pimPage.btnPersonalBloodTypeAPlus.click();
        pimPage.btnPersonalSaveCustom.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // Attachments
        // First
        pimPage.btnPersonalAddAttachment.click();
        waitElementToLoad(pimPage.btnPersonalSaveAttachment);
        pimPage.filePersonalAttachment.sendKeys(System.getProperty("user.dir") +
                TestData.PROFILE_FILE1_PATH);
        pimPage.textAreaPersonalComment.sendKeys(TestData.EMPLOYEE_ATTACHMENT_COMMENT);
        pimPage.btnPersonalSaveAttachment.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // Second
        pimPage.btnPersonalAddAttachment.click();
        waitElementToLoad(pimPage.btnPersonalSaveAttachment);
        pimPage.filePersonalAttachment.sendKeys(System.getProperty("user.dir") +
                TestData.PROFILE_FILE2_PATH);
        pimPage.textAreaPersonalComment.sendKeys(TestData.EMPLOYEE_ATTACHMENT_COMMENT);
        pimPage.btnPersonalSaveAttachment.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        Assert.assertTrue(pimPage.btnPersonalAttachmentEdit.isDisplayed(), "First attachment is missing.");
        Assert.assertTrue(pimPage.btnPersonalAttachmentDelete.isDisplayed(), "Second attachment is missing.");

        // Edit attachment
        pimPage.btnPersonalAttachmentEdit.click();
        waitElementToLoad(pimPage.btnPersonalSaveEditAttachment);
        pimPage.filePersonalAttachment.sendKeys(System.getProperty("user.dir") +
                TestData.PROFILE_FILE3_PATH);
        fluentWaitElementInvisible(pimPage.elementLoader);
        pimPage.btnPersonalSaveEditAttachment.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        Assert.assertTrue(pimPage.btnPersonalAttachment3Edit.isDisplayed(), "Edited attachment is missing.");

        // Download attachment
        // ToDo: look for info how to test download and finish this test
//        pimPage.btnPersonalAttachmentDownload.click();

        // Delete attachment
        pimPage.btnPersonalAttachmentDelete.click();
        waitElementToLoad(pimPage.btnPersonalAttachmentConfirmDelete);
        pimPage.btnPersonalAttachmentConfirmDelete.click();
        Assert.assertEquals(pimPage.elementPersonalAttachmentRow.size(), 1,
                "More than one attachment found.");

    }

    @Test
    public void addCandidate() throws InterruptedException {
        login();
        waitElementToLoad(mainPage.btnMenuRecruitment);
        mainPage.btnMenuRecruitment.click();
        waitElementToLoad(recPage.btnAddCandidate);
        recPage.btnAddCandidate.click();
        waitElementToLoad(recPage.fieldRecruitmentFirstName);
        recPage.fieldRecruitmentFirstName.sendKeys(TestData.RECRUITMENT_FIRST_NAME);
        recPage.fieldRecruitmentMiddleName.sendKeys(TestData.RECRUITMENT_MIDDLE_NAME);
        recPage.fieldRecruitmentLastName.sendKeys(TestData.RECRUITMENT_LAST_NAME);
        recPage.selectRecruitmentVacancy.click();
        waitElementToLoad(recPage.btnRecruitmentVacancyJunior);
        recPage.btnRecruitmentVacancyJunior.click();
        recPage.fieldRecruitmentEmail.sendKeys(TestData.RECRUITMENT_EMAIL);
        recPage.fieldRecruitmentContactNumber.sendKeys(TestData.RECRUITMENT_NUMBER);
        recPage.fileRecruitmentAttachment.sendKeys(System.getProperty("user.dir") +
                TestData.PROFILE_FILE1_PATH);
        recPage.fieldRecruitmentKeywords.sendKeys(TestData.RECRUITMENT_KEYWORD);
        recPage.textAreaRecruitmentNotes.sendKeys(TestData.RECRUITMENT_NOTES);
        recPage.checkRecruitmentConsent.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        recPage.btnRecruitmentSave.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // Shortlist Candidate
        waitElementToLoad(recPage.btnRecruitmentShortlistSchedule);
        recPage.btnRecruitmentShortlistSchedule.click();
        recPage.textAreaRecruitmentNotes.sendKeys(TestData.RECRUITMENT_NOTES);
        fluentWaitElementInvisible(pimPage.elementLoader);
        recPage.btnRecruitmentSaveShortlist.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // Schedule Interview
        recPage.btnRecruitmentShortlistSchedule.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        waitElementToLoad(recPage.fieldRecruitmentInterviewTitle);
        recPage.fieldRecruitmentInterviewTitle.sendKeys(TestData.RECRUITMENT_INTERVIEW_TITLE);
        recPage.fieldRecruitmentInterviewer.sendKeys("a");
        waitElementToLoad(recPage.btnRecruitmentInterviewer);
        recPage.fieldRecruitmentInterviewerDate.sendKeys(TestData.RECRUITMENT_INTERVIEW_DATE);
        recPage.fieldRecruitmentInterviewerTime.click();
        recPage.textAreaRecruitmentNotes.sendKeys(TestData.RECRUITMENT_NOTES);
        fluentWaitElementInvisible(pimPage.elementLoader);
        recPage.btnRecruitmentSaveScheduleInterview.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        Assert.assertEquals(recPage.textRecruitmentStatus.getText(), "Interview Scheduled",
                "Recruitment interview status is not correct.");
        // ToDo: test when all data is in the system

//        Thread.sleep(5000);
    }

    @Test
    public void addNewBuzz() throws InterruptedException {
        login();
        waitElementToLoad(mainPage.btnMenuBuzz);
        mainPage.btnMenuBuzz.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // create new post
        waitElementToLoad(buzzPage.textAreaBuzzComment);
        buzzPage.textAreaBuzzComment.sendKeys(TestData.BUZZ_COMMENT);
        buzzPage.btnBuzzSharePhoto.click();
        fluentWaitElementVisible(buzzPage.elementBuzzEditModal);
        buzzPage.fileBuzzAddPhoto.sendKeys(System.getProperty("user.dir") + TestData.BUZZ_PHOTO_PATH);
        buzzPage.btnBuzzShare.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        buzzPage.btnBuzzLike.click();
        driver.navigate().refresh();
        fluentWaitElementInvisible(pimPage.elementLoader);
        Assert.assertEquals(buzzPage.textBuzzAfterLike.getText(), "1 Like",
                "Buzz post should have 1 Like.");
        // edit post
        buzzPage.btnBuzzThreeDots.click();
        waitElementToLoad(buzzPage.btnBuzzThreeDotsEdit);
        buzzPage.btnBuzzThreeDotsEdit.click();
        waitElementToLoad(buzzPage.elementBuzzEditModal);
        buzzPage.textAreaBuzzEditModalComment.click();
        buzzPage.textAreaBuzzEditModalComment.clear();
        buzzPage.textAreaBuzzEditModalComment.sendKeys(TestData.BUZZ_COMMENT_EDIT);
        buzzPage.btnBuzzEditModalPost.click();
        // post comment
        fluentWaitElementInvisible(pimPage.elementLoader);
        buzzPage.btnBuzzComment.click();
        waitElementToLoad(buzzPage.fieldBuzzCommentText);
        buzzPage.fieldBuzzCommentText.sendKeys(TestData.BUZZ_COMMENT_ON_POST);
        buzzPage.fieldBuzzCommentText.sendKeys(Keys.RETURN);
        buzzPage.btnBuzzCommentLike.click();
        Assert.assertTrue(buzzPage.elementBuzzCommentLikeHeart.isDisplayed(),
                "Heart is not displayed when comment is liked.");
        // edit post comment
        buzzPage.btnBuzzCommentEdit.click();
        waitElementToLoad(buzzPage.fieldBuzzCommentEdit);
        buzzPage.fieldBuzzCommentEdit.sendKeys(TestData.BUZZ_COMMENT_EDIT);
        buzzPage.fieldBuzzCommentEdit.sendKeys(Keys.RETURN);
        // delete post comment
        buzzPage.btnBuzzCommentDelete.click();
        fluentWaitElementVisible(buzzPage.btnBuzzCommentModalDelete);
        buzzPage.btnBuzzCommentModalDelete.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        // delete post
        buzzPage.btnBuzzThreeDotsEdited.click();
        waitElementToLoad(buzzPage.btnBuzzThreeDotsDelete);
        buzzPage.btnBuzzThreeDotsDelete.click();
        fluentWaitElementVisible(buzzPage.btnBuzzCommentModalDelete);
        buzzPage.btnBuzzCommentModalDelete.click();
        Assert.assertEquals(buzzPage.textBuzzPostDeleteToast.getText(), "Successfully Deleted",
                "Post deletion message not displayed");
    }

    @Test
    public void addNewReport() throws InterruptedException {
        login();
        waitElementToLoad(mainPage.btnMenuPim);
        mainPage.btnMenuPim.click();
        waitElementToLoad(reportPage.btnPimReports);
        reportPage.btnPimReports.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        reportPage.btnReportsAdd.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        reportPage.fieldReportName.sendKeys(TestData.REPORT_NAME);
        // Criteria 1
        reportPage.selectReportCriteria.click();
        waitElementToLoad(reportPage.btnReportCriteriaEducation);
        reportPage.btnReportCriteriaEducation.click();
        reportPage.btnReportCriteriaPlus.click();
        // Criteria 2
        reportPage.selectReportCriteria.click();
        waitElementToLoad(reportPage.btnReportCriteriaPayGrade);
        reportPage.btnReportCriteriaPayGrade.click();
        reportPage.btnReportCriteriaPlus.click();
        // Criteria Delete
        reportPage.btnReportCriteriaDelete.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        reportPage.btnReportCriteriaDelete.click();
        // Include
        reportPage.selectReportInclude.click();
        waitElementToLoad(reportPage.btnReportIncludeCape);
        reportPage.btnReportIncludeCape.click();

        // Fields Personal
        reportPage.selectReportFieldGroup.click();
        waitElementToLoad(reportPage.btnReportFieldGroupPersonal);
        reportPage.btnReportFieldGroupPersonal.click();
        // Display
        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldPersonalEmployeeId);
        reportPage.btnReportDisplayFieldPersonalEmployeeId.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldPersonalEmployeeLastName);
        reportPage.btnReportDisplayFieldPersonalEmployeeLastName.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldPersonalEmployeeFirstName);
        reportPage.btnReportDisplayFieldPersonalEmployeeFirstName.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        // Fields Contact Details
        reportPage.selectReportFieldGroup.click();
        waitElementToLoad(reportPage.btnReportFieldGroupContactDetails);
        reportPage.btnReportFieldGroupContactDetails.click();
        // Display
        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldContactDetailsAddress);
        reportPage.btnReportDisplayFieldContactDetailsAddress.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldContactDetailsHomeTelephone);
        reportPage.btnReportDisplayFieldContactDetailsHomeTelephone.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldContactDetailsMobile);
        reportPage.btnReportDisplayFieldContactDetailsMobile.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldContactDetailsWorkTelephone);
        reportPage.btnReportDisplayFieldContactDetailsWorkTelephone.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldContactDetailsWorkEmail);
        reportPage.btnReportDisplayFieldContactDetailsWorkEmail.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        // Fields Dependants
        reportPage.selectReportFieldGroup.click();
        waitElementToLoad(reportPage.btnReportFieldGroupDependents);
        reportPage.btnReportFieldGroupDependents.click();
        // Display
        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldDependentsName);
        reportPage.btnReportDisplayFieldDependentsName.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldDependentsRelationship);
        reportPage.btnReportDisplayFieldDependentsRelationship.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldDependentsDateOfBirth);
        reportPage.btnReportDisplayFieldDependentsDateOfBirth.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldDependentsSeqNo);
        reportPage.btnReportDisplayFieldDependentsSeqNo.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        // Fields Emergency Contacts
        reportPage.selectReportFieldGroup.click();
        waitElementToLoad(reportPage.btnReportFieldGroupEmergencyContacts);
        reportPage.btnReportFieldGroupEmergencyContacts.click();

        // Display
        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldEmergencyContactsName);
        reportPage.btnReportDisplayFieldEmergencyContactsName.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldEmergencyContactsHomeTelephone);
        reportPage.btnReportDisplayFieldEmergencyContactsHomeTelephone.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldEmergencyContactsWorkTelephone);
        reportPage.btnReportDisplayFieldEmergencyContactsWorkTelephone.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldEmergencyContactsRelationship);
        reportPage.btnReportDisplayFieldEmergencyContactsRelationship.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        reportPage.selectReportDisplayField.click();
        waitElementToLoad(reportPage.btnReportDisplayFieldEmergencyContactsMobile);
        reportPage.btnReportDisplayFieldEmergencyContactsMobile.click();
        reportPage.btnReportDisplayFieldsPlus.click();

        // Include Header
        for (WebElement element : reportPage.switchReportIncludeHeader) {
            element.click();
        }

        // Delete Fields
        reportPage.btnReportXSeqNo.click();
        reportPage.btnReportXMobile.click();
        reportPage.btnReportXAddress.click();
        // Delete Group
        reportPage.btnReportDeleteGroupEmergencyContacts.click();

        Assert.assertEquals(reportPage.elementsFieldsLeft.size(), 9, "There must be 9 fields left");
        reportPage.btnReportSave.click();
        fluentWaitElementInvisible(pimPage.elementLoader);
        Assert.assertTrue(reportPage.elementReportTableHeader.isDisplayed(),
                "Saving report should redirect to created report page.");
    }

    private static void waitElementToLoad(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    private void fluentWaitElementInvisible(WebElement webElement) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(1));
//                .ignoring(NoSuchElementException.class)
        wait.until(ExpectedConditions.invisibilityOf(webElement));
    }

    private void fluentWaitElementVisible(WebElement webElement) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }
}
