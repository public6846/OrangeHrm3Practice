package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PimPage {


    // PIM list
    @FindBy(xpath = "//button[@class='oxd-button oxd-button--medium oxd-button--secondary']")
    public WebElement btnAddPim;

    @FindBy(xpath = "//label[text() = 'Supervisor Name']/../../div[2]")
    public WebElement fieldPimSupervisorName;

    @FindBy(xpath = "//label[text() = 'Supervisor Name']/../..//span[text() = 'Odis  Adalwin']/..")
    public WebElement btnListSupervisorNameOdis;

    @FindBy(xpath = "//button[contains(@class, 'orangehrm-left-space')]")
    public WebElement btnListPimSearch;

    @FindBy(xpath = "//div[@class='oxd-table-card']/div/div[3]")
    public WebElement textPimEmployeeLastNameInList;



    // PIM add new employee

    @FindBy(xpath = "//input[@type='file']")
    public WebElement fileAddPicture;

    @FindBy(xpath = "//input[@name='firstName']")
    public WebElement fieldPimFirstName;

    @FindBy(xpath = "//input[@name='lastName']")
    public WebElement fieldPimLastName;

    @FindBy(xpath = "//div[contains(@class, 'switch')]")
    public WebElement switchLoginDetails;

    @FindBy(xpath = "//div[@class='oxd-form-loader']")
    public WebElement elementLoader;

    @FindBy(xpath = "//label[contains(text(), 'Username')]/../..//input")
    public WebElement fieldPimUsername;

    @FindBy(xpath = "//label[text() = 'Password']/../..//input")
    public WebElement fieldPimPassword;

    @FindBy(xpath = "//label[contains(text(), 'Confirm')]/../..//input")
    public WebElement fieldPimPasswordConfirm;

    @FindBy(xpath = "//button[@type='submit']")
    public WebElement btnPimSave;

    // PIM user edit menu

    @FindBy(xpath = "//a[contains(@href, 'Job')]")
    public WebElement btnPimJob;

    @FindBy(xpath = "//a[contains(@href, 'ReportTo')]")
    public WebElement btnPimReportTo;



    // PIM employee job

    @FindBy(xpath = "//label[text() = 'Joined Date']/../..//input")
    public WebElement fieldPimJoinDate;

    @FindBy(xpath = "//label[text() = 'Job Title']/../../div[2]")
    public WebElement fieldPimJobTitle;

    @FindBy(xpath = "//label[text() = 'Job Title']/../..//span[text() = 'Chief Executive Officer']/..")
    public WebElement btnPimJobTitleCEO;

    @FindBy(xpath = "//label[text() = 'Job Category']/../../div[2]")
    public WebElement fieldPimJobCategory;

    @FindBy(xpath = "//label[text() = 'Job Category']/../..//span[text() = 'Craft Workers']/..")
    public WebElement btnPimJobCategoryCW;

    @FindBy(xpath = "//label[text() = 'Location']/../../div[2]")
    public WebElement fieldPimJobLocation;

    @FindBy(xpath = "//label[text() = 'Location']/../..//span[text() = 'New York Sales Office']/..")
    public WebElement btnPimJobLocationNYSO;

    @FindBy(xpath = "//label[text() = 'Employment Status']/../../div[2]")
    public WebElement fieldPimEmploymentStatus;

    @FindBy(xpath = "//label[text() = 'Employment Status']/../..//span[text() = 'Full-Time Permanent']/..")
    public WebElement btnPimEmploymentStatusFTP;

    @FindBy(xpath = "//h6[text()='Job Details']/..//button")
    public WebElement btnPimJobDetailsSave;



    // PIM employee report-to
    @FindBy(xpath = "//h6[text() = 'Assigned Supervisors']/../button")
    public WebElement btnReportAddSupervisor;

    @FindBy(xpath = "//label[text() = 'Name']/../..//input")
    public WebElement fieldReportSupervisorName;

    @FindBy(xpath = "//label[text() = 'Name']/../..//span[text() = 'Odis  Adalwin']/..")
    public WebElement btnReportSupervisorNameOdis;

    @FindBy(xpath = "//label[text() = 'Reporting Method']/../../div[2]")
    public WebElement fieldReportSupervisorMethod;

    @FindBy(xpath = "//label[text() = 'Reporting Method']/../..//span[text() = 'Direct']/..")
    public WebElement btnReportSupervisorMethodDirect;

    @FindBy(xpath = "//button[@type= 'submit']")
    public WebElement btnReportSupervisorSave;



    // PIM personal details form
    @FindBy(xpath = "//input[@name='firstName']")
    public WebElement fieldPersonalFirstName;

    @FindBy(xpath = "//input[@name='middleName']")
    public WebElement fieldPersonalMiddleName;

    @FindBy(xpath = "//input[@name='lastName']")
    public WebElement fieldPersonalLastName;

    @FindBy(xpath = "//label[text() = 'Nickname']/../..//input")
    public WebElement fieldPersonalNickname;

    @FindBy(xpath = "//label[text() = 'Other Id']/../..//input")
    public WebElement fieldPersonalOtherId;

    @FindBy(xpath = "//label[text() = \"Driver's License Number\"]/../..//input")
    public WebElement fieldPersonalDriverLicenseNumber;

    @FindBy(xpath = "//label[text() = 'License Expiry Date']/../..//input")
    public WebElement selectPersonalLicenseExpiry;

    @FindBy(xpath = "//label[text() = 'SSN Number']/../..//input")
    public WebElement fieldPersonalSsnNumber;

    @FindBy(xpath = "//label[text() = 'SIN Number']/../..//input")
    public WebElement fieldPersonalSinNumber;

    @FindBy(xpath = "//label[text() = 'Nationality']/../../div[2]")
    public WebElement selectPersonalNationality;

    @FindBy(xpath = "//label[text() = 'Nationality']/../..//span[contains(text(), 'Albanian')]/..")
    public WebElement btnPersonalNationalityAlbanian;

    @FindBy(xpath = "//label[text() = 'Marital Status']/../../div[2]")
    public WebElement selectPersonalMaritalStatus;

    @FindBy(xpath = "//label[text() = 'Marital Status']/../..//span[text() = 'Other']/..")
    public WebElement btnPersonalMaritalStatusOther;

    @FindBy(xpath = "//label[text() = 'Date of Birth']/../..//input")
    public WebElement fieldPersonalDateOfBirth;

    @FindBy(xpath = "//label[.//input[@value='1']]")
    public WebElement radioPersonalGender;

    @FindBy(xpath = "//label[text() = 'Military Service']/../..//input")
    public WebElement fieldPersonalMilitaryService;

    @FindBy(xpath = "//label[text() = 'Smoker']/../..//span/i")
    public WebElement checkPersonalSmoker;

    @FindBy(xpath = "//h6[text()='Personal Details']/..//button[@type= 'submit']")
    public WebElement btnPersonalSaveDetails;

    @FindBy(xpath = "//label[text() = 'Blood Type']/../../div[2]")
    public WebElement selectPersonalBloodType;

    @FindBy(xpath = "//label[text() = 'Blood Type']/../..//span[text() = 'A+']/..")
    public WebElement btnPersonalBloodTypeAPlus;

    @FindBy(xpath = "//h6[text()='Custom Fields']/..//button[@type= 'submit']")
    public WebElement btnPersonalSaveCustom;

    // Attachments
    @FindBy(xpath = "//input[@type='file']")
    public WebElement filePersonalAttachment;

    @FindBy(xpath = "//label[text() = 'Comment']/../..//textarea")
    public WebElement textAreaPersonalComment;

    @FindBy(xpath = "//h6[text()='Add Attachment']/..//button[@type= 'submit']")
    public WebElement btnPersonalSaveAttachment;

    @FindBy(xpath = "//h6[text()='Edit Attachment']/..//button[@type= 'submit']")
    public WebElement btnPersonalSaveEditAttachment;

    @FindBy(xpath = "//button[contains(@class, 'oxd-button--text')]")
    public WebElement btnPersonalAddAttachment;

    @FindBy(xpath = "//div[text()='testtxt1.txt']/../..//i[contains(@class,  'bi-pencil-fill')]/..")
    public WebElement btnPersonalAttachmentEdit;

    @FindBy(xpath = "//div[text()='testtxt3.txt']/../..//i[contains(@class,  'bi-pencil-fill')]/..")
    public WebElement btnPersonalAttachment3Edit;

    @FindBy(xpath = "//div[text()='testtxt2.txt']/../..//i[contains(@class,  'bi-trash')]/..")
    public WebElement btnPersonalAttachmentDelete;

    @FindBy(xpath = "//div[text()='testtxt1.txt']/../..//i[contains(@class,  'bi-download')]/..")
    public WebElement btnPersonalAttachmentDownload;

    @FindBy(xpath = "//button[contains(@class, 'oxd-button--label-danger')]")
    public WebElement btnPersonalAttachmentConfirmDelete;

    @FindAll({@FindBy(xpath = "//div[@class='oxd-table-card']")})
    public List<WebElement> elementPersonalAttachmentRow;

    public PimPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
