package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RecruitmentPage {

    // Recruitment list

    @FindBy(xpath = "//button[@class='oxd-button oxd-button--medium oxd-button--secondary']")
    public WebElement btnAddCandidate;

    // Recruitment add candidate form

    @FindBy(xpath = "//input[@name='firstName']")
    public WebElement fieldRecruitmentFirstName;

    @FindBy(xpath = "//input[@name='middleName']")
    public WebElement fieldRecruitmentMiddleName;

    @FindBy(xpath = "//input[@name='lastName']")
    public WebElement fieldRecruitmentLastName;

    @FindBy(xpath = "//label[text() = 'Vacancy']/../../div[2]")
    public WebElement selectRecruitmentVacancy;

    @FindBy(xpath = "//label[text() = 'Vacancy']/../..//span[contains(text(), 'Junior Account Assistant')]/..")
    public WebElement btnRecruitmentVacancyJunior;

    @FindBy(xpath = "//label[text() = 'Email']/../..//input")
    public WebElement fieldRecruitmentEmail;

    @FindBy(xpath = "//label[text() = 'Contact Number']/../..//input")
    public WebElement fieldRecruitmentContactNumber;

    @FindBy(xpath = "//input[@type='file']")
    public WebElement fileRecruitmentAttachment;

    @FindBy(xpath = "//label[text() = 'Keywords']/../..//input")
    public WebElement fieldRecruitmentKeywords;

    @FindBy(xpath = "//label[text() = 'Notes']/../..//textarea")
    public WebElement textAreaRecruitmentNotes;

    @FindBy(xpath = "//label[text() = 'Consent to keep data']/../..//i[contains(@class,'checkbox')]")
    public WebElement checkRecruitmentConsent;

    @FindBy(xpath = "//h6[text()='Add Candidate']/..//button[@type= 'submit']")
    public WebElement btnRecruitmentSave;

    // Recruitment edit candidate form

    @FindBy(xpath = "//h6[text()='Application Stage']/..//button[contains(@class, 'oxd-button--success')]")
    public WebElement btnRecruitmentShortlistSchedule;

    @FindBy(xpath = "//h6[text()='Shortlist Candidate']/..//button[@type= 'submit']")
    public WebElement btnRecruitmentSaveShortlist;

    // Recruitment schedule interview

    @FindBy(xpath = "//label[text() = 'Interview Title']/../..//input")
    public WebElement fieldRecruitmentInterviewTitle;

    @FindBy(xpath = "//label[text() = 'Interviewer']/../..//input")
    public WebElement fieldRecruitmentInterviewer;

    @FindBy(xpath = "//label[text() = 'Interviewer']/../..//span[contains(text(), 'a')]/..")
    public WebElement btnRecruitmentInterviewer;

    @FindBy(xpath = "//label[text() = 'Date']/../..//input")
    public WebElement fieldRecruitmentInterviewerDate;

    @FindBy(xpath = "//label[text() = 'Time']/../..//input")
    public WebElement fieldRecruitmentInterviewerTime;

    @FindBy(xpath = "//h6[text()='Schedule Interview']/..//button[@type= 'submit']")
    public WebElement btnRecruitmentSaveScheduleInterview;

    @FindBy(xpath = "//h6[text()='Application Stage']/..//div[contains(@class,'status')]/p")
    public WebElement textRecruitmentStatus;


    public RecruitmentPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
