package lt.viko.eif.orangehrm3practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ReportPage {

    // PIM Report page

    @FindBy(xpath = "//a[text()='Reports']/..")
    public WebElement btnPimReports;

    @FindBy(xpath = "//h5[text()='Employee Reports']/../../../..//button[@class='oxd-button " +
            "oxd-button--medium oxd-button--secondary']")
    public WebElement btnReportsAdd;

    // PIM Report form

    @FindBy(xpath = "//label[text() = 'Report Name']/../..//input")
    public WebElement fieldReportName;

    @FindBy(xpath = "//label[text() = 'Selection Criteria']/../..//div[@class='oxd-select-wrapper']")
    public WebElement selectReportCriteria;

    @FindBy(xpath = "//label[text() = 'Selection Criteria']/../..//span[text() = 'Education']/..")
    public WebElement btnReportCriteriaEducation;

    @FindBy(xpath = "//label[text() = 'Selection Criteria']/../..//span[text() = 'Pay Grade']/..")
    public WebElement btnReportCriteriaPayGrade;

    @FindBy(xpath = "//label[text() = 'Include']/../..//div[@class='oxd-select-wrapper']")
    public WebElement selectReportInclude;

    @FindBy(xpath = "//label[text() = 'Include']/../..//span[text() = 'Current and Past Employees']/..")
    public WebElement btnReportIncludeCape;

    @FindBy(xpath = "//h6[text()='Selection Criteria']/..//i[contains(@class,'bi-plus')]/..")
    public WebElement btnReportCriteriaPlus;

    @FindBy(xpath = "//i[@class='oxd-icon bi-trash-fill']/..")
    public WebElement btnReportCriteriaDelete;

    @FindBy(xpath = "//label[text() = 'Select Display Field Group']/../..//div[@class='oxd-select-wrapper']")
    public WebElement selectReportFieldGroup;

    @FindBy(xpath = "//label[text() = 'Select Display Field Group']/../..//span[text() = 'Personal']/..")
    public WebElement btnReportFieldGroupPersonal;

    @FindBy(xpath = "//label[text() = 'Select Display Field Group']/../..//span[text() = 'Contact Details']/..")
    public WebElement btnReportFieldGroupContactDetails;

    @FindBy(xpath = "//label[text() = 'Select Display Field Group']/../..//span[text() = 'Dependents']/..")
    public WebElement btnReportFieldGroupDependents;

    @FindBy(xpath = "//label[text() = 'Select Display Field Group']/../..//span[text() = 'Emergency Contacts']/..")
    public WebElement btnReportFieldGroupEmergencyContacts;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//div[@class='oxd-select-wrapper']")
    public WebElement selectReportDisplayField;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Employee Id']/..")
    public WebElement btnReportDisplayFieldPersonalEmployeeId;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Employee Last Name']/..")
    public WebElement btnReportDisplayFieldPersonalEmployeeLastName;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Employee First Name']/..")
    public WebElement btnReportDisplayFieldPersonalEmployeeFirstName;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Address']/..")
    public WebElement btnReportDisplayFieldContactDetailsAddress;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Home Telephone']/..")
    public WebElement btnReportDisplayFieldContactDetailsHomeTelephone;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Mobile']/..")
    public WebElement btnReportDisplayFieldContactDetailsMobile;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Work Telephone']/..")
    public WebElement btnReportDisplayFieldContactDetailsWorkTelephone;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Work Email']/..")
    public WebElement btnReportDisplayFieldContactDetailsWorkEmail;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Name']/..")
    public WebElement btnReportDisplayFieldDependentsName;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Relationship']/..")
    public WebElement btnReportDisplayFieldDependentsRelationship;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Date of Birth']/..")
    public WebElement btnReportDisplayFieldDependentsDateOfBirth;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'SeqNo']/..")
    public WebElement btnReportDisplayFieldDependentsSeqNo;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Name']/..")
    public WebElement btnReportDisplayFieldEmergencyContactsName;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Home Telephone']/..")
    public WebElement btnReportDisplayFieldEmergencyContactsHomeTelephone;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Work Telephone']/..")
    public WebElement btnReportDisplayFieldEmergencyContactsWorkTelephone;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Relationship']/..")
    public WebElement btnReportDisplayFieldEmergencyContactsRelationship;

    @FindBy(xpath = "//label[text() = 'Select Display Field']/../..//span[text() = 'Mobile']/..")
    public WebElement btnReportDisplayFieldEmergencyContactsMobile;

    @FindBy(xpath = "//h6[text()='Display Fields']/..//i[contains(@class,'bi-plus')]/..")
    public WebElement btnReportDisplayFieldsPlus;

    // ToDo: take as array and iterate over all elements

    @FindAll({@FindBy(xpath = "//div[@class='oxd-switch-wrapper']")})
    public List<WebElement> switchReportIncludeHeader;

    @FindBy(xpath = "//span[contains(text(),'SeqNo')]/i")
    public WebElement btnReportXSeqNo;

    @FindBy(xpath = "//span[contains(text(),'Mobile')]/i")
    public WebElement btnReportXMobile;

    @FindBy(xpath = "//span[contains(text(),'Address')]/i")
    public WebElement btnReportXAddress;

    @FindBy(xpath = "//p[text()='Emergency Contacts']/../button")
    public WebElement btnReportDeleteGroupEmergencyContacts;

    @FindAll({@FindBy(xpath = "//span[contains(@class, 'chip')]")})
    public List<WebElement> elementsFieldsLeft;

    @FindBy(xpath = "//button[@type='submit']")
    public WebElement btnReportSave;

    @FindBy(xpath = "//div[@class='oxd-report-table-header']")
    public WebElement elementReportTableHeader;

    public ReportPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
