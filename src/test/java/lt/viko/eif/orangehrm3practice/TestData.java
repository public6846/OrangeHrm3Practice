package lt.viko.eif.orangehrm3practice;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TestData {

    static DateTimeFormatter forLogin = DateTimeFormatter.ofPattern("HHmmss");
    static DateTimeFormatter forCurrentDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    static DateTimeFormatter forCurrentTime = DateTimeFormatter.ofPattern("HH:mm");
    static LocalDateTime now = LocalDateTime.now();

    // PIM
    public static final String PROFILE_PICTURE_PATH = "\\testfiles\\testprofile.jpg";
    public static final String PROFILE_FILE1_PATH = "\\testfiles\\testtxt1.txt";
    public static final String PROFILE_FILE2_PATH = "\\testfiles\\testtxt2.txt";
    public static final String PROFILE_FILE3_PATH = "\\testfiles\\testtxt3.txt";
    public static final String EMPLOYEE_USERNAME = "Kal789635" + forLogin.format(now);
    public static final String EMPLOYEE_PASSWORD = "Te5t1452" + forLogin.format(now);
    public static final String EMPLOYEE_FIRST_NAME = "Kalyan" + forLogin.format(now);
    public static final String EMPLOYEE_LAST_NAME = "Chuldah" + forLogin.format(now);
    public static final String EMPLOYEE_JOIN_DATE = forCurrentDate.format(now.minusDays(10));
    public static final String SUPERVISOR_FULL_NAME = "Odis Adalwin";

    public static final String EMPLOYEE_FIRST_NAME_EDITED = "Edit";
    public static final String EMPLOYEE_MIDDLE_NAME_EDITED = "MiddleEdit" + forLogin.format(now);
    public static final String EMPLOYEE_LAST_NAME_EDITED = "Edit";
    public static final String EMPLOYEE_NICKNAME = "User" + forLogin.format(now);
    public static final String EMPLOYEE_OTHER_ID = "665464AW55";
    public static final String EMPLOYEE_DRIVER_LICENSE_NUMBER = "11223333";
    public static final String EMPLOYEE_LICENSE_EXPIRY_DATE = forCurrentDate.format(now.plusMonths(23));
    public static final String EMPLOYEE_SSN_NUMBER = "00400505479";
    public static final String EMPLOYEE_SIN_NUMBER = "6464141";
    public static final String EMPLOYEE_DATE_OF_BIRTH = forCurrentDate.format(now.minusYears(30));
    public static final String EMPLOYEE_MILITARY_SERVICE = "Yes";
    public static final String EMPLOYEE_ATTACHMENT_COMMENT = "Comment for an attachment!";

    // RECRUITMENT
    public static final String RECRUITMENT_FIRST_NAME = "Rich" + forLogin.format(now);
    public static final String RECRUITMENT_MIDDLE_NAME = "Junior" + forLogin.format(now);
    public static final String RECRUITMENT_LAST_NAME = "Smith" + forLogin.format(now);
    public static final String RECRUITMENT_EMAIL = "rjs@exampleqwert.dev";
    public static final String RECRUITMENT_NUMBER = "00354635466363";
    public static final String RECRUITMENT_KEYWORD = "junior";
    public static final String RECRUITMENT_NOTES = "Junior notes for testing!";
    public static final String RECRUITMENT_INTERVIEW_TITLE = "Juniors initial interview";
    public static final String RECRUITMENT_INTERVIEW_DATE = forCurrentDate.format(now.plusDays(5));

    // BUZZ
    public static final String BUZZ_COMMENT = "BuzzTest123";
    public static final String BUZZ_PHOTO_PATH = "\\testfiles\\testpic.jpg";
    public static final String BUZZ_COMMENT_EDIT = "Edit";
    public static final String BUZZ_COMMENT_ON_POST = "New Comment";

    // PIM REPORT
    public static final String REPORT_NAME = "Report Test987";
}
